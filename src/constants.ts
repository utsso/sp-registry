export const ENTITIES_OPEN_TAG = `<md:EntitiesDescriptor ID="ut-sp-metadata_2024523T93550" 
Name="ut-sp-metadata" xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata" 
xmlns:mdrpi="urn:oasis:names:tc:SAML:metadata:rpi" 
xmlns:mdui="urn:oasis:names:tc:SAML:metadata:ui" 
xmlns:idpdisc="urn:oasis:names:tc:SAML:profiles:SSO:idp-discovery-protocol" 
xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:ds="http://www.w3.org/2000/09/xmldsig#" 
xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" 
xmlns:wayf="http://sdss.ac.uk/2006/06/WAYF" 
xmlns:xi="http://www.w3.org/2001/XInclude" 
xmlns:mdattr="urn:oasis:names:tc:SAML:metadata:attribute" 
xmlns:auth="http://docs.oasis-open.org/wsfed/authorization/200706"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xenc="http://www.w3.org/2001/04/xmlenc#" xmlns:alg="urn:oasis:names:tc:SAML:metadata:algsupport" 
xmlns:shibmd="urn:mace:shibboleth:metadata:1.0" xmlns:init="urn:oasis:names:tc:SAML:profiles:SSO:request-init" 
xmlns="urn:oasis:names:tc:SAML:2.0:metadata" 
validUntil="2024-06-13T09:35:50Z">
</md:EntitiesDescriptor>
`

export const NAMESPACE_AND_URI: {[key: string]: string}= {
    "xmlns:md": "urn:oasis:names:tc:SAML:2.0:metadata",
    "xmlns:mdrpi": "urn:oasis:names:tc:SAML:metadata:rpi",
    "xmlns:mdui": "urn:oasis:names:tc:SAML:metadata:ui",
    "xmlns:idpdisc": "urn:oasis:names:tc:SAML:profiles:SSO:idp-discovery-protocol",
    "xmlns:xml": "http://www.w3.org/XML/1998/namespace",
    "xmlns:ds": "http://www.w3.org/2000/09/xmldsig#",
    "xmlns:saml": "urn:oasis:names:tc:SAML:2.0:assertion",
    "xmlns:wayf": "http://sdss.ac.uk/2006/06/WAYF",
    "xmlns:xi": "http://www.w3.org/2001/XInclude",
    "xmlns:mdattr": "urn:oasis:names:tc:SAML:metadata:attribute",
    "xmlns:auth": "http://docs.oasis-open.org/wsfed/authorization/200706",
    "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
    "xmlns:xenc": "http://www.w3.org/2001/04/xmlenc#",
    "xmlns:alg": "urn:oasis:names:tc:SAML:metadata:algsupport",
    "xmlns:shibmd": "urn:mace:shibboleth:metadata:1.0",
    "xmlns:init": "urn:oasis:names:tc:SAML:profiles:SSO:request-init"
  }

export const ENTITY_EXTENSION_ATTR_NAMES:
    { [key: string]: { position: number, displayName: string } } = {
    "https://saml.utoronto.ca/entity-attribute/releaseAttributes,urn:oasis:names:tc:SAML:2.0:attrname-format:uri": { displayName: "Attributes Release", position: 0 },
    "http://macedir.org/entity-category,urn:oasis:names:tc:SAML:2.0:attrname-format:uri": { displayName: "Service Provider Categories", position: 1 },
    "https://saml.utoronto.ca/entity-attribute/duoProfile,urn:oasis:names:tc:SAML:      2.0:attrname-format:uri": { displayName: "Duo MFA Configuration", position: 2 }
}

export const DUO_MFA_CONFIG_VALS = ["bypass", "standard", "hybrid", "enhanced"]

export const ENTITY_CATEGORIES: {
    [key: string]: string
} = {
    "https://saml.utoronto.ca/entity-category/internal": "internal",
    "https://saml.utoronto.ca/entity-category/external": "external",
    "http://id.incommon.org/category/registered-by-incommon": "incommon"
}

export const RELEASE_REQUEST_ATTRS: {
    [key: string]: {
        friendlyNames: string[];
        saml2Name: string;
        displayName?: string;
        tooltip?: string;
    };
} = {
    "eduPersonPrincipalName": {
        "friendlyNames": ["eppn", "eduPersonPrincipalName"],
        "saml2Name": "urn:oid:1.3.6.1.4.1.5923.1.1.1.6",
        "tooltip": `Unique scoped identifier for a user in UofT's administrative domain in the format of user@scope. For UofT, scope is
        utoronto.ca (our administrative domain), user is a UTORid. Example: zhaojon2@utoronto.ca`
    },
    "durableM365": {
        "friendlyNames": ["mail", "email"],
        "saml2Name": "urn:oid:0.9.2342.19200300.100.1.3"
    },
    "surname": {
        "friendlyNames": ["sn", "surname"],
        "saml2Name": "urn:oid:2.5.4.4"
    },
    "givenName": {
        "friendlyNames": ["givenName"],
        "saml2Name": "urn:oid:2.5.4.42",
    },
    "UTORid": {
        "friendlyNames": ["UTORid"],
        "saml2Name": "urn:oid:1.3.6.1.4.1.15465.3.1.8",
        "tooltip": "Unique string identifying a person affiliated with UofT e.g. students, staff etc. Example: zhaojon2"
    },
    "commonName": {
        "friendlyNames": ["cn", "commonName"],
        "saml2Name": "urn:oid:2.5.4.3",
    },
    "eduPersonAffiliation": {
        "friendlyNames": ["eduPersonAffiliation"],
        "saml2Name": "urn:oid:1.3.6.1.4.1.5923.1.1.1.1",
    },
    "primaryCampus": {
        "friendlyNames": ["primaryCampus"],
        "saml2Name": "urn:oid:1.3.6.1.4.1.15465.3.1.407",
    },
    "primaryDivision": {
        "friendlyNames": ["primaryDivision"],
        "saml2Name": "urn:oid:1.3.6.1.4.1.15465.3.1.237",
    },
    "eduPersonScopedAffiliation": {
        "friendlyNames": ["eduPersonScopedAffiliation"],
        "saml2Name": "urn:oid:1.3.6.1.4.1.5923.1.1.1.9"
    },
    "empnum": {
        "friendlyNames": ["empnum"],
        "saml2Name": "urn:oid:1.3.6.1.4.1.15465.3.1.6",
    },
    "personid": {
        "friendlyNames": ["personid"],
        "saml2Name": "urn:oid:1.3.6.1.4.1.15465.3.1.5",
    },
    "displayName": {
        "friendlyNames": ["displayName"],
        "saml2Name": "urn:oid:2.16.840.1.113730.3.1.241",
    },
    "UTid": {
        "friendlyNames": ["UTid"],
        "saml2Name": "urn:oid:1.3.6.1.4.1.15465.3.1.1",
    },
    "isactivestaff": {
        "friendlyNames": ["isactivestaff"],
        "saml2Name": "isactivestaff",
    },
    "isactivefaculty": {
        "friendlyNames": ["isactivefaculty"],
        "saml2Name": "isactivefaculty",
    },
    "isstudent": {
        "friendlyNames": ["isstudent"],
        "saml2Name": "isstudent",
    },
    "isaffiliate": {
        "friendlyNames": ["isaffiliate"],
        "saml2Name": "isaffiliate",
    },
    "isMemberOf": {
        "friendlyNames": ["isMemberOf"],
        "saml2Name": "urn:oid:1.3.6.1.4.1.5923.1.5.1.1",
    }
};

export const UI_INFO_FIELDS = ["mdui:DisplayName", "mdui:Description", "mdiui:InformationURL", "mdui:PrivacyStatementURL", "mdui:Logo"]

export const ASSERTION_BINDINGS: { [key: string]: string } = {
    "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST": "HTTP-POST",
    "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign": "HTTP-POST-SimpleSign",
    "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact": "HTTP-Artifact",
    "urn:oasis:names:tc:SAML:2.0:bindings:PAOS": "PAOS"
}

export const KEEP_SAME_LINE_TAGS = ["saml:AttributeValue", ...UI_INFO_FIELDS.map((str)=>{
    return "mdui:"+str
}), "md:OrganizationName", "md:OrganizationDisplayName", "md:OrganizationURL", "ds:KeyName", "ds:X509SubjectName", "md:GivenName", "md:EmailAddress", "md:SurName"]

export const CONTACT_TYPES = ["technical", "support", "administrative", "billing", "other"]

export const BASE_FORM: FormShape = {
    tags: {
        "mdattr:EntityAttributes": { 0:{name: "mdattr:EntityAttributes", children: {}, attributes: {}} },
        "md:KeyDescriptor": {},
        "md:Organization": {
            0:{
            name: "md:Organization", attributes: {}, children: {
                0: { name: "md:OrganizationName", children: "", attributes: {} },
                1: { name: "md:OrganizationDisplayName", children: "", attributes: {} },
                2: { name: "md:OrganizationURL", children: "", attributes: {} }
            }
        }},
        "init:RequestInitiator":{},
        "md:AssertionConsumerService": {},
        "md:ContactPerson":{"0":{
            attributes:{
                "contactType":"string"
            },
            name:"md:ContactPerson",
            children:{
                0: { name: "md:GivenName", children: "", attributes: {} },
                1: { name: "md:SurName", children: "", attributes: {} },
                2: { name: "md:EmailAddress", children: "", attributes: {} }
            }
        }}
    },
    attributes: {
        entityId: "",
    }
}