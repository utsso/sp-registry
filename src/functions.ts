"use client"
import { ChangeEvent } from "react";
import { NAMESPACE_AND_URI, ENTITY_EXTENSION_ATTR_NAMES, RELEASE_REQUEST_ATTRS, DUO_MFA_CONFIG_VALS, ENTITY_CATEGORIES, BASE_FORM } from "./constants";
export const certToText = async (formEvent: ChangeEvent<HTMLInputElement>): Promise<string> => {
    const file = formEvent.target.files?.item(0);

    if (file && (file.type === "application/x-pem-file" || "application/x-x509-ca-cert")) {

        const reader = new FileReader();
        const fileEnding = file.name.split(".").at(-1)
        let fileContent = ''
        await new Promise<void>((resolve, reject) => {
            console.log(fileEnding)
            if (fileEnding !== "der") {
                reader.onload = function (e) {
                    const pemText = e.target?.result;
                    if (typeof pemText !== "string") {
                        throw new Error("file was not read as string")
                    }
                    fileContent = removePemHeaders(pemText);

                    resolve()
                };
                reader.readAsText(file);
            } else {
                reader.onload = function (e) {
                    const derArrayBuf = e.target?.result
                    if (!(derArrayBuf instanceof ArrayBuffer)) {
                        throw new Error("file was not read as byte array")
                    }
                    fileContent = Buffer.from(derArrayBuf).toString("base64")
                    resolve()
                }
                reader.readAsArrayBuffer(file)
            }
        })
        return fileContent
    } else {
        throw new Error("Not a valid .crt, .cer, .pem, or .der file")
    }
}


const removePemHeaders = (pem: string) => {

    const lines = pem.split('\n');

    const pemBody = lines.filter(line =>
        line.trim() &&
        !line.includes('---')
    );

    return pemBody.join('');
}

export const xmlFileUploadToText = async (formEvent: ChangeEvent<HTMLInputElement>): Promise<string> => {
    const file = formEvent.target.files?.item(0);

    if (file && file.type === 'text/xml') {
        const reader = new FileReader();
        let fileContent = ""

        await new Promise<void>((resolve, reject): void => {
            reader.onload = function (readEvent): void {
                const readResult = readEvent.target?.result
                if (typeof readResult !== "string") {
                    throw new Error("xml file was not read as string")
                }
                fileContent = readResult
                resolve()
            };
            reader.readAsText(file)
        })
        return fileContent
    } else {
        throw new Error("Not a valid xml file")
    }
}


export const processXMLText = async (fileContent: string) => {
    fileContent =
        `<md:EntitiesDescriptor ID="ut-sp-metadata_2024523T93550" 
        Name="ut-sp-metadata" xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata" 
        xmlns:mdrpi="urn:oasis:names:tc:SAML:metadata:rpi" 
        xmlns:mdui="urn:oasis:names:tc:SAML:metadata:ui" 
        xmlns:idpdisc="urn:oasis:names:tc:SAML:profiles:SSO:idp-discovery-protocol" 
        xmlns:xml="http://www.w3.org/XML/1998/namespace" 
        xmlns:ds="http://www.w3.org/2000/09/xmldsig#" 
        xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" 
        xmlns:wayf="http://sdss.ac.uk/2006/06/WAYF" 
        xmlns:xi="http://www.w3.org/2001/XInclude" 
        xmlns:mdattr="urn:oasis:names:tc:SAML:metadata:attribute" 
        xmlns:auth="http://docs.oasis-open.org/wsfed/authorization/200706"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:xenc="http://www.w3.org/2001/04/xmlenc#" xmlns:alg="urn:oasis:names:tc:SAML:metadata:algsupport" 
        xmlns:shibmd="urn:mace:shibboleth:metadata:1.0" xmlns:init="urn:oasis:names:tc:SAML:profiles:SSO:request-init" 
        xmlns="urn:oasis:names:tc:SAML:2.0:metadata" 
        validUntil="2024-06-13T09:35:50Z">`
        + fileContent + `</md:EntitiesDescriptor>`


    const parser = new DOMParser()
    const xmlDoc = parser.parseFromString(fileContent, "text/xml")
    let formData: FormShape = JSON.parse(JSON.stringify(BASE_FORM))
    const MULTI_TAGS = ["md:AssertionConsumerService", "init:RequestInitiator", "md:KeyDescriptor"] as const
    for (const i of Object.keys(formData.tags)) {
        const res = xmlDoc.getElementsByTagName(i)
        console.log(i)
        console.log(res)
        if (res.length > 0) {
            let jsonTags: XmlTag[] = []
            for (let j = 0; j < res.length; j++) {
                const tag = res.item(j)
                if (tag) {
                    jsonTags.push(parseDOMTag(tag))
                }
            }
            console.log(jsonTags)

            if (MULTI_TAGS.includes(i as typeof MULTI_TAGS[number])) {
                console.log("multi")
                console.log(i)
                jsonTags.forEach((tag) => {
                    const newKey = crypto.randomUUID()
                    type TagShape = FormShape['tags'][typeof MULTI_TAGS[number]][string]
                    formData.tags[i as (typeof MULTI_TAGS)[number]][newKey] = tag as TagShape
                })
            } else {
                type TagShape = FormShape['tags'][keyof FormShape['tags']]["0"]
                formData.tags[i as keyof FormShape['tags']]["0"] = jsonTags[0] as TagShape

            }
        }
    }
    const entityId = xmlDoc.getElementsByTagName("EntityDescriptor").item(0)?.getAttribute("entityID")
    if (typeof entityId === "string") {
        formData.attributes.entityId = entityId
    }
    console.log("hello")
    console.log(formData)
    return formData
}

const parseDOMTag = (domElem: Element) => {
    const jsonTag: XmlTag = { name: '', attributes: {} }
    const attrs = domElem.attributes
    jsonTag.name = domElem.tagName
    for (let i = 0; i < attrs.length; i++) {
        const attr = attrs.item(i)
        if (!attr) {
            throw new Error(`No attribute at index${i}`)
        }
        const attrVal = attr.value
        jsonTag.attributes[attr.name] = attrVal
    }
    const childNodes = domElem.children
    const nodeText = domElem.textContent?.trim()
    if (childNodes.length > 0) {
        jsonTag.children = {}
        for (let i = 0; i < childNodes.length; i++) {
            const child = childNodes.item(i)
            if (!child) {
                throw new Error(`No child element at index ${i}`)
            }
            const childJsonTag = parseDOMTag(child)
            jsonTag.children[i] = childJsonTag
        }
    } else if (nodeText) {
        jsonTag.children = nodeText
    }
    return jsonTag
}

const serializeXMLTagToDOM = (tag: XmlTag, document: Document) => {
    const domTag = document.createElement(tag.name)
    Object.keys(tag.attributes).forEach((attr) => {
        console.log(attr)
        domTag.setAttribute(attr, tag.attributes[attr])
    })
    if (typeof tag.children === "string") {
        domTag.textContent = tag.children
    } else if (typeof tag.children === "object") {
        Object.keys(tag.children).forEach((key) => {
            if (typeof tag.children === "object") {
                const child = serializeXMLTagToDOM(tag.children[key], document)
                domTag.appendChild(child)
            }
        })
    }
    return domTag
}


export const XmlJSONToText = (values: FormShape) => {
    const xmlTagJson = createEntityDescTag(values)
    const doc = new Document()
    const domTree = serializeXMLTagToDOM(xmlTagJson, doc)
    doc.append(domTree)

    const xmlSerializer = new XMLSerializer()
    let xsltDoc = new DOMParser().parseFromString([
        // describes how we want to modify the XML - indent everything
        `<xsl:stylesheet version="1.0"
            xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
            <xsl:output omit-xml-declaration="yes" indent="yes"/>
            <xsl:template match="node()|@*">
                <xsl:copy>
                    <xsl:apply-templates select="node()|@*"/>
                </xsl:copy>
            </xsl:template>
        </xsl:stylesheet>`
    ].join('\n'), 'text/xml');

    let xsltProcessor = new XSLTProcessor();
    xsltProcessor.importStylesheet(xsltDoc);
    let prettyDoc = xsltProcessor.transformToDocument(doc)
    return xmlSerializer.serializeToString(prettyDoc)
}

export const entityValAutocompleteOpts = (attrName: string) => {
    let options: { label: string, value: string }[] = [];
    switch (attrName) {
        case 'User Attributes Requested for Release':
            options = Object.keys(RELEASE_REQUEST_ATTRS).map((reqAttrId) => {
                return { label: reqAttrId, value: reqAttrId }
            })
            break;
        case 'Duo MFA Configuration':
            options = DUO_MFA_CONFIG_VALS.map((configVal) => {
                return { label: configVal, value: configVal }
            })
            break;
        case 'Service Provider Categories':
            options = Object.keys(ENTITY_CATEGORIES).map((categoryVal) => {
                const labelName = ENTITY_CATEGORIES[categoryVal]
                return { label: labelName, value: categoryVal }
            })
            break;
        default:
            options = [{ label: '', value: '' }]
    }
    return options
}

export const createEntityDescTag = (values: FormShape) => {
    let entityAttrsCopy: EntityAttributes = JSON.parse(JSON.stringify(values.tags["mdattr:EntityAttributes"][0]))
    let assertionCopy: {[key: string]: AssertionConsumerService} = JSON.parse(JSON.stringify(values.tags["md:AssertionConsumerService"]))

    Object.keys(entityAttrsCopy.children).map((fieldKey) => {
        const { Name: name, NameFormat: nameFormat } = entityAttrsCopy.children[fieldKey].attributes
        if (name) {
            delete entityAttrsCopy.children[fieldKey]
            const position = ENTITY_EXTENSION_ATTR_NAMES[name + "," + nameFormat].position
            entityAttrsCopy.children[position] = values.tags["mdattr:EntityAttributes"][0].children[fieldKey]
        }
    })

    Object.keys(assertionCopy).map((key, index)=>{
        assertionCopy[key].attributes.index = index.toString()
    })

    console.log(entityAttrsCopy)
    const entityDescExtensions: XmlTag = {
        name: "md:Extensions", children: {
            0: entityAttrsCopy
        }, attributes: {}
    }

    const spsodExtensions: XmlTag = {
        name: "md:Extensions", children: {
            ...values.tags["init:RequestInitiator"]
        },
        attributes: {}
    }

    const spsodTag: XmlTag = {
        name: "SPSSODescriptor", attributes: { protocolSupportEnumeration: "urn:oasis:names:tc:SAML:2.0:protocol" },
        children: {
            0: spsodExtensions,
            ...values.tags["md:KeyDescriptor"],
            ...assertionCopy
        }
    }

    const entityDescriptorTag: XmlTag = {
        name: "md:EntityDescriptor", attributes: {
            "entityID": values.attributes.entityId,
            ...NAMESPACE_AND_URI
        },
        children: {
            0: entityDescExtensions,
            1: spsodTag,
            2: values.tags["md:Organization"][0],
            3: values.tags["md:ContactPerson"][0]
        },
    }
    return entityDescriptorTag
}