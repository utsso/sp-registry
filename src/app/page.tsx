
'use client'
import { Formik } from "formik";
import { useEffect, useState } from "react";
import { processXMLText, xmlFileUploadToText, XmlJSONToText } from "../functions";
import Image from 'next/image'
import uoftLogo from "../assets/logo.svg"
import { Button } from "@mui/material";
import { MetadataFormFields, XmlDisplay } from "@/components";
import { BASE_FORM } from "@/constants";
export default function Home() {
  const [formShapeState, setFormShapeState] = useState<FormShape>(JSON.parse(JSON.stringify(BASE_FORM)))
  const [activeDisplay, setActiveDisplay] = useState<"form"|"xml">("form")

  const displayChangeHandler = () => {
    if (activeDisplay === "form") {
      (document.getElementById("xml-display") as HTMLElement).style.display = "none";
      (document.getElementById("form-display") as HTMLElement).style.display = "flex"
    } else {
      (document.getElementById("xml-display") as HTMLElement).style.display = "flex";
      (document.getElementById("form-display") as HTMLElement).style.display = "none"
    }
  }

  useEffect(() => {
    let mql = window.matchMedia("(max-width: 1280px)");
    if (mql.matches) {
      displayChangeHandler()
    }
    mql.addEventListener("change", (e) => {
      if (!e.matches) {
        (document.getElementById("xml-display") as HTMLElement).style.display = "flex";
        (document.getElementById("form-display") as HTMLElement).style.display = "flex"
      } else {
        displayChangeHandler()
      }
    })
  }, [activeDisplay])

  return (
    <div className="w-full">
      <Formik
        enableReinitialize={true}
        initialValues={formShapeState}
        onSubmit={(values) => {
          console.log("ghefewfw")
          const xmlText = XmlJSONToText(values);
          const url = URL.createObjectURL(new Blob([xmlText]));
          const downloadElem = document.getElementById("xml-download") as HTMLLinkElement
          downloadElem.href = url
          console.log(url)
          downloadElem.click()
        }}
      >
        {({ handleSubmit, submitForm, values, handleChange, setFieldValue }) => {

          return (
            <div className="flex flex-col mt-20">
              <div className="flex flex-wrap justify-between">
                <div className="mb-10 flex justify-center grow-2 basis-0 w-full pl-6 pr-6">
                  <div>
                  <h2 className="text-3xl w-fit text-center">Import metadata to autofill "Advanced Info"<br/><span className="text-sm">
                    (This is required if you're not manually filling in the "Advanced Info" section below. You should be able 
                    to automatically generate for your service an XML file that has all the required metadata to correctly autofill the "Advanced Info" section.) 
                    </span></h2>
                    </div>
                  <Button
                    component="label"
                    className="add-button ml-8 h-fit"
                    sx={{
                      textTransform: "capitalize",
                      '&:hover': {
                        backgroundColor: "#09265c"
                      },
                    }}
                  >
                    Import
                    <input type="file" accept=".xml" hidden id="xmlUpload" className="text-md" onChange={async (e) => {
                      const xmlText = await xmlFileUploadToText(e)
                      const xmlDataForForm = await processXMLText(xmlText)
                      setFormShapeState(xmlDataForForm)

                    }} />
                  </Button>
                </div>
                <div className="mb-10 flex justify-center max-xl:grow-2 grow-3 basis-0">
                  <h2 className="text-3xl w-fit">Export metadata</h2>
                  <button type="submit" onClick={submitForm} className="add-button ml-8 h-fit z-10">Export</button>
                  <a download="metadata.xml" className="hidden" id="xml-download"></a>
                </div>
              </div>
              <div className="max-xl:flex hidden w-full justify-center">
                <button className="add-button my-12" onClick={() => {
                  if (activeDisplay === "form") {
                    setActiveDisplay("xml")
                  } else {
                    setActiveDisplay("form")
                  }
                }}>Show {activeDisplay === "form" ? "generated XML" : "metadata input fields"}</button>
              </div>
              <div className="flex">
                <div className="grow-2 basis-0 max-xl:hidden flex flex-col" id="form-display">
                  <MetadataFormFields handleSubmit={handleSubmit} handleChange={handleChange} values={values} setFieldValue={setFieldValue} />
                </div>
                <div className="grow-3 basis-0 flex flex-col items-center max-xl:hidden" id="xml-display">
                  <h2 className="text-5xl mb-8">Metadata XML</h2>
                  <div className="pl-12">
                    <XmlDisplay values={values} />
                  </div>
                </div>
              </div>
            </div>
          )
        }}
      </Formik>
    </div>
  );
}
