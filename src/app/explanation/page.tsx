"use client"
import { RELEASE_REQUEST_ATTRS } from "@/constants"
import { FieldDropdown } from "@/components"
export default function Home() {
    return <div className="flex w-full flex-col p-24 gap-y-8">
        <p>
            Entity Attributes are used to describe how your service interacts with UofT’s Identity Provider (IdP) system. There are two Entity Attributes you can add to your metadata: “Attributes Release” and “Duo Profile”.
        </p>
        <p>
            The <b>“Attributes Release”</b> Entity Attribute is the set of a user’s UofT IdP attributes (e.g. UTORid) your service requests to access via UofT’s IdP system. You can view a complete list of UofT IdP attributes your service can possibly request and what information they contain below.
        </p>
        <div>
            <FieldDropdown closedAtStart={true} Header={<span className="text-xl">UofT IdP Attributes</span>}>
                <div className="pl-14 flex flex-col gap-y-8 mt-4">
                    {Object.keys(RELEASE_REQUEST_ATTRS).sort().map((key) => {
                        const header = RELEASE_REQUEST_ATTRS[key].friendlyNames.at(-1) as string
                        return <div>
                        <FieldDropdown closedAtStart={true} Header={<span>{header}</span>}>
                            <p className="ml-12">{RELEASE_REQUEST_ATTRS[key].tooltip}</p>
                        </FieldDropdown>
                        </div>
                    })}
                </div>
            </FieldDropdown>
        </div>
        <p>
            The <b>“Duo Profile”</b> Entity Attribute is a list of ways your service can interact with UofT’s Duo MFA service. Each value in the list describes a way your service can interact with Duo. The complete list of values and their meanings can be viewed below.
        </p>
        <div>

        </div>
    </div>
}
