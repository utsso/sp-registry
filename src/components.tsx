import { FormEvent, ReactNode, useState } from "react"
import { ENTITY_EXTENSION_ATTR_NAMES, ASSERTION_BINDINGS, KEEP_SAME_LINE_TAGS, CONTACT_TYPES } from "./constants"
import { entityValAutocompleteOpts, certToText, createEntityDescTag } from "./functions"
import { Autocomplete, Button, TextField} from "@mui/material"



const ServiceFunctionalityAttributesFields = (props: { values: FormShape, setFieldValue: SetFieldValueType }) => {
  const { values, setFieldValue } = props
  console.log(values)
  let entityAttrsCopy: EntityAttributes = JSON.parse(JSON.stringify(values.tags["mdattr:EntityAttributes"][0]))
  console.log(entityAttrsCopy)

  return <div className="pl-8 flex flex-col items-start">
    {Object.keys(values.tags["mdattr:EntityAttributes"][0].children).map((key) => {
      const nameAndFormat = !entityAttrsCopy.children[key].attributes.Name ? '' :
        entityAttrsCopy.children[key].attributes.Name + "," + entityAttrsCopy.children[key].attributes.NameFormat

      return <div key={`entityAttribute${key}`} className="mb-20">
        <div>
          <button className="mr-5" type="button" onClick={() => {
            delete entityAttrsCopy.children[key]
            setFieldValue('tags["mdattr:EntityAttributes"][0]', entityAttrsCopy)
          }}>X</button>
          <label className="label-text" htmlFor={`entityAttribute${key}AttrType`}>Attribute</label>
          <select name={`entityAttribute${key}AttrType`}
            id={`entityAttribute${key}AttrType`}
            value={nameAndFormat}
            style={{ border: "1px solid #3a3a3a" }}
            onChange={(e) => {
              const [name, format] = e.target.value.split(",")
              entityAttrsCopy.children[key].attributes.Name = name
              entityAttrsCopy.children[key].attributes.NameFormat = format
              entityAttrsCopy.children[key].children = {}
              setFieldValue('tags["mdattr:EntityAttributes"][0]', entityAttrsCopy)
            }}>
            <option value="" disabled hidden>Select Attribute Type</option>
            {Object.keys(ENTITY_EXTENSION_ATTR_NAMES).map((entityAttrStr) => {
              const displayName = ENTITY_EXTENSION_ATTR_NAMES[entityAttrStr].displayName
              return <option value={entityAttrStr} key={entityAttrStr}>{displayName}</option>
            })}
          </select>
        </div>
        {nameAndFormat !== "" ?
          <div className="flex flex-col gap-y-6 mt-8 pl-8">
            <div className="pl-6">
              {Object.keys(values.tags["mdattr:EntityAttributes"][0].children[key].children).map((attrValKey) => {
                let attrValIdent = `entityAttribute${key}Val${attrValKey}`


                const attrDisplayName = ENTITY_EXTENSION_ATTR_NAMES[nameAndFormat].displayName
                const options = entityValAutocompleteOpts(attrDisplayName)
                const fieldVal = values.tags["mdattr:EntityAttributes"][0].children[key].children[attrValKey]
                const curOpt = options.filter((val) => {
                  return val.value.toLowerCase() === fieldVal.children.toLowerCase()
                }).at(-1)
                const txt = attrDisplayName === "Attributes Release" ? "IdP Attribute" : "Value"
                return <div key={attrValIdent} className="mb-6 flex">
                  <button className="mr-5" type="button" onClick={() => {
                    delete entityAttrsCopy.children[key].children[attrValKey]
                    setFieldValue('tags["mdattr:EntityAttributes"][0]', entityAttrsCopy)
                  }}>X</button>
                  <Autocomplete options={options}
                    isOptionEqualToValue={(option, value) => {
                      return value.value.toLowerCase() === option.value.toLowerCase()
                    }}

                    value={curOpt}
                    inputValue={curOpt?.label}
                    onChange={(e, newValue) => {
                      entityAttrsCopy.children[key].children[attrValKey] = newValue ? {
                        name: "saml:AttributeValue", children: newValue.value, attributes: {}
                      } : { name: "saml:AttributeValue", children: '', attributes: {} }
                      setFieldValue('tags["mdattr:EntityAttributes"][0]', entityAttrsCopy)
                    }}
                    sx={{
                      width: "250px",
                      "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline":
                      {
                        border: "1px solid black"
                      },
                      "& .MuiInputLabel-root.Mui-focused": {
                        color: "black"
                      }
                    }} renderInput={(params) => <TextField {...params} style={{ border: "none" }} label="Value" />} />
                </div>

              })}
              <button className="add-button" type="button" onClick={() => {
                const newKey = crypto.randomUUID()
                entityAttrsCopy.children[key].children[newKey] = { name: "saml:AttributeValue", children: '', attributes: {} }
                setFieldValue('tags["mdattr:EntityAttributes"][0]', entityAttrsCopy)
              }}>Add {}</button>
            </div>
          </div>
          :
          null
        }
      </div>
    })}
    <button
      type="button"
      className="add-button"
      onClick={() => {
        const newKey = crypto.randomUUID()
        entityAttrsCopy.children[newKey] = { name: "saml:Attribute", children: {}, attributes: { Name: '', NameFormat: '' } }
        setFieldValue('tags["mdattr:EntityAttributes"][0]', entityAttrsCopy)
      }}>Add Attribute</button>
      <p>An explanation on how Entity Attributes works can be found <a href="/explanation">here</a></p>
  </div>
}

const RequestInitiatorFields = (props: { values: FormShape, setFieldValue: SetFieldValueType }) => {
  const { values, setFieldValue } = props
  let requestInitiatorCopy: { [key: string]: RequestInitiator } = JSON.parse(JSON.stringify(values.tags["init:RequestInitiator"]))
  return <div className="pl-8">
    {Object.keys(values.tags["init:RequestInitiator"]).map((fieldKey) => {
      return <div key={fieldKey} className="mb-5">
        <button className="mr-5 inline" type="button" onClick={() => {
          delete requestInitiatorCopy[fieldKey]
          setFieldValue('tags["init:RequestInitiator"]', requestInitiatorCopy)
        }}>X</button>
        <label className="label-text" htmlFor={`discoveryResponses${fieldKey}`}>Location</label>
        <input type="text" id={`discoveryResponses${fieldKey}`} className="text-field" value={values.tags["init:RequestInitiator"][fieldKey].attributes.Location} onChange={(e) => {
          requestInitiatorCopy[fieldKey].attributes.Location = e.target.value
          setFieldValue("tags['init:RequestInitiator']", requestInitiatorCopy)
        }} />
      </div>
    })}
    <button
      type="button"
      className="add-button"
      onClick={() => {
        const newKey = crypto.randomUUID()

        requestInitiatorCopy[newKey] = {
          name: "init:RequestInitiator",
          attributes: { Binding: "urn:oasis:names:tc:SAML:profiles:SSO:request-init", Location: "" }
        }
        setFieldValue('tags["init:RequestInitiator"]', requestInitiatorCopy)
      }}>Add Endpoint</button>
  </div>
}

const KeyDescriptorsFields = (props: { values: FormShape, setFieldValue: SetFieldValueType }) => {
  const { values, setFieldValue } = props
  const keysCopy: { [key: string]: KeyDescriptor } = JSON.parse(JSON.stringify(values.tags['md:KeyDescriptor']))
  console.log(keysCopy)
  return <div className="pl-8 flex gap-y-4 flex-col items-start">
    {Object.keys(values.tags['md:KeyDescriptor']).map((fieldKey, index) => {
      const keyInfo: KeyInfo = keysCopy[fieldKey].children[0]
      return <FieldDropdown key={`keyDesc${fieldKey}`}
        Header={<div className="flex items-center">
          <button className="mr-5 inline" type="button" onClick={() => {
            delete keysCopy[fieldKey]
            setFieldValue('tags["md:KeyDescriptor"]', keysCopy)
          }}>X</button>
          <h4 className="label-text">Key {index + 1}</h4>
        </div>}
      >
        <div className="pl-16 flex flex-col gap-y-4 mb-12">
          <div>
            <label className="label-text" htmlFor={`keyDesc${fieldKey}Use`}>Use</label>
            <select name={`keyDesc${fieldKey}Use`}
              id={`keyDesc${fieldKey}Use`}
              value={keysCopy[fieldKey].attributes.use}
              style={{ border: "1px solid #3a3a3a" }}
              onChange={(e) => {
                keysCopy[fieldKey].attributes.use = e.target.value
                setFieldValue('tags["md:KeyDescriptor"]', keysCopy)
              }}>
              <option value="">N/A</option>
              <option value="encryption">Encryption</option>
              <option value="signing">Signing</option>
            </select>
          </div>
          <div>
            <FieldDropdown Header={<label className="label-text">Key Names</label>}>
              <div className="pl-8 gap-y-4 flex flex-col mt-4">
                {Object.keys(keyInfo.children).filter((key) => keyInfo.children[key].name === "ds:KeyName").map((key) => {
                  return <div>
                    <button className="mr-5 inline" type="button" onClick={() => {
                      delete keysCopy[fieldKey].children[0].children[key]
                      setFieldValue('tags["md:KeyDescriptor"]', keysCopy)
                    }}>X</button>
                    <label className="label-text" htmlFor={`keyDesc${key}Name`}>Name</label>
                    <input type="text" name={`keyDesc${key}Name`}
                      id={`keyDesc${key}Name`}
                      value={(keysCopy[fieldKey].children[0].children[key] as TextTag).children}
                      className="text-field"
                      onChange={(e) => {
                        (keysCopy[fieldKey].children[0].children[key] as TextTag).children = e.target.value
                        setFieldValue(`tags["md:KeyDescriptor"]`, keysCopy)
                      }}>
                    </input>
                  </div>
                })}
                <button
                  type="button"
                  className="add-button"
                  onClick={() => {
                    const newKey = crypto.randomUUID()
                    keysCopy[fieldKey].children[0].children[newKey] = { name: "ds:KeyName", attributes: {}, children: "" }
                    setFieldValue('tags["md:KeyDescriptor"]', keysCopy)
                  }}>Add Key Name</button>
              </div>
            </FieldDropdown>
          </div>
          <div>
            <FieldDropdown Header={<label className="label-text">X509 Data</label>}>
              <div className="pl-8 flex flex-col mt-4">
                {Object.keys(keyInfo.children).filter((infoKey) => keyInfo.children[infoKey].name === "ds:X509Data").map((infoKey) => {
                  const x509data = keyInfo.children[infoKey] as X509Data

                  return <div className="pl-12 flex flex-col mt-4 gap-y-4 mb-4">
                    <div>
                      <button className="mr-5 inline" type="button" onClick={() => {
                        delete keysCopy[fieldKey].children[0].children[infoKey as keyof KeyInfo]
                        setFieldValue('tags["md:KeyDescriptor"]', keysCopy)
                      }}>X</button>
                      <label className="label-text">Data</label>
                    </div>
                    <div className="pl-16 flex flex-col gap-y-4">
                      <label className="label-text">X509 Subject Names</label>
                      <div className="pl-8 flex flex-col">
                        {Object.keys(x509data.children).filter(dataKey => x509data.children[dataKey].name === "ds:X509SubjectName").map((dataKey) => {
                          return <div className="flex flex-col mb-4">
                            <div>
                              <button className="mr-5 inline" type="button" onClick={() => {
                                delete (keysCopy[fieldKey].children[0].children[infoKey as keyof KeyInfo] as X509Data).children[dataKey]
                                setFieldValue('tags["md:KeyDescriptor"]', keysCopy)
                              }}>X</button>
                              <label className="label-text" htmlFor={`keyDesc${dataKey}SubName`}>Subject Name</label>
                              <input type="text" name={`keyDesc${dataKey}SubName`}
                                id={`keyDesc${dataKey}SubName`}
                                value={(keysCopy[fieldKey].children[0].children[infoKey as keyof KeyInfo] as X509Data).children[dataKey].children}
                                className="text-field"
                                onChange={(e) => {
                                  (keysCopy[fieldKey].children[0].children[infoKey as keyof KeyInfo] as X509Data).children[dataKey].children = e.target.value
                                  setFieldValue(`tags["md:KeyDescriptor"]`, keysCopy)
                                }}>
                              </input>
                            </div>
                          </div>
                        })
                        }
                        <button
                          type="button"
                          className="add-button"
                          onClick={() => {
                            const newKey = crypto.randomUUID();
                            console.log(keysCopy[fieldKey].children[0].children[infoKey as keyof KeyInfo] as X509Data);
                            (keysCopy[fieldKey].children[0].children[infoKey as keyof KeyInfo] as X509Data).children[newKey] = { name: "ds:X509SubjectName", children: "", attributes: {} }
                            setFieldValue('tags["md:KeyDescriptor"]', keysCopy)
                          }}>Add X509 Subject Name</button>
                      </div>
                    </div>
                    <div className="pl-16 flex flex-col my-4">
                      {Object.keys(x509data.children).filter(dataKey => x509data.children[dataKey].name === "ds:X509Certificate").map((dataKey) => {
                        return <div>
                          <label className="label-text">X509 Certificate</label>
                          <Button
                            component="label"
                            className="add-button mx-8"
                            sx={{
                              textTransform: "capitalize",
                              '&:hover': {
                                backgroundColor: "#09265c"
                              },
                            }}
                          >
                            Upload File
                            <input accept=".pem, .crt, .cer, .der" hidden id={`x509Cert${fieldKey}`} type="file" onChange={async (e) => {
                              const res = await certToText(e);
                              (keysCopy[fieldKey].children[0].children[infoKey as keyof KeyInfo] as X509Data).children[dataKey].children = res
                              setFieldValue("tags['md:KeyDescriptor']", keysCopy)
                            }} />
                          </Button>
                        </div>
                      })
                      }
                    </div>
                  </div>
                })}
                <button
                  type="button"
                  className="add-button"
                  onClick={() => {
                    const newKey = crypto.randomUUID()
                    const newKey2 = crypto.randomUUID()
                    let children: { [key: string]: TextTag } = {}
                    children[newKey2] = { name: "ds:X509Certificate", attributes: {}, children: "" }
                    keysCopy[fieldKey].children[0].children[newKey] = { name: "ds:X509Data", attributes: {}, children: children }
                    setFieldValue('tags["md:KeyDescriptor"]', keysCopy)
                  }}>Add X509 Data</button>
              </div>
            </FieldDropdown>
          </div>
          <div>
            <FieldDropdown Header={<label className="label-text">Encryption Methods</label>}>
              <div className="pl-8 gap-y-4 flex flex-col mt-4">
                {Object.keys(keysCopy[fieldKey].children).filter((key) => key !== "0").map((key) => {
                  return <div>
                    <button className="mr-5 inline" type="button" onClick={() => {
                      delete keysCopy[fieldKey].children[key]
                      setFieldValue('tags["md:KeyDescriptor"]', keysCopy)
                    }}>X</button>
                    <label className="label-text" htmlFor={`keyDesc${key}EncryptMethod`}>Algorithm</label>
                    <input type="text" name={`keyDesc${key}EncryptMethod`}
                      id={`keyDesc${key}EncryptMethod`}
                      value={(keysCopy[fieldKey].children[key] as EncryptionMethod).attributes.Algorithm}
                      className="text-field"
                      onChange={(e) => {
                        (keysCopy[fieldKey].children[key] as EncryptionMethod).attributes.Algorithm = e.target.value
                        setFieldValue(`tags["md:KeyDescriptor"]`, keysCopy)
                      }}>
                    </input>
                  </div>
                })}
                <button
                  type="button"
                  className="add-button"
                  onClick={() => {
                    const newKey = crypto.randomUUID()
                    keysCopy[fieldKey].children[newKey] = { name: "md:EncryptionMethod", attributes: { Algorithm: "" } }
                    setFieldValue('tags["md:KeyDescriptor"]', keysCopy)
                  }}>Add Encryption Method</button>
              </div>
            </FieldDropdown>
          </div>
        </div>
      </FieldDropdown>
    })}
    <button
      type="button"
      className="add-button"
      onClick={() => {
        const newKey = crypto.randomUUID()
        keysCopy[newKey] = {
          name: "md:KeyDescriptor", attributes: {},
          children: {
            0:
              {
                name: "ds:KeyInfo",
                children: {

                },
                attributes: {}
              } as KeyInfo,
          }
        } as KeyDescriptor
        setFieldValue('tags["md:KeyDescriptor"]', keysCopy)
      }}>Add Key</button>
  </div>
}

const AssertionConsumerServicesFields = (props: { values: FormShape, setFieldValue: SetFieldValueType }) => {
  const { values, setFieldValue } = props
  let assertionConsumersCopy: { [key: string]: AssertionConsumerService } = JSON.parse(JSON.stringify(values.tags['md:AssertionConsumerService']))
  console.log(assertionConsumersCopy)
  return <div className="pl-8">
    {Object.keys(values.tags['md:AssertionConsumerService']).map((fieldKey, index) => {
      console.log(assertionConsumersCopy[fieldKey].attributes)
      return <div key={fieldKey} className="mb-8">
        <div className="flex items-center mb-3">
          <button className="mr-5 inline" type="button" onClick={() => {
            delete assertionConsumersCopy[fieldKey]
            setFieldValue("tags['md:AssertionConsumerService']", assertionConsumersCopy)
          }}>X</button>
          <h4 className="label-text w-fit">Endpoint {index + 1}</h4>
        </div>
        <div className="pl-16">
          <div>
            <label className="label-text" htmlFor={`assertionConsumerBinding${fieldKey}`}>Binding</label>
            <select name={`assertionConsumerLocation${fieldKey}`}
              id={`assertionConsumerLocation${fieldKey}`}
              value={assertionConsumersCopy[fieldKey].attributes.Binding}
              style={{ border: "1px solid #3a3a3a" }}
              onChange={(e) => {
                assertionConsumersCopy[fieldKey].attributes.Binding = e.target.value
                setFieldValue("tags['md:AssertionConsumerService']", assertionConsumersCopy)
              }}>
              <option value="" disabled hidden>Select a Binding</option>
              {Object.keys(ASSERTION_BINDINGS).map((bindingVal) => {
                return <option value={bindingVal} key={bindingVal}>{ASSERTION_BINDINGS[bindingVal]}</option>
              })}
            </select>
          </div>
          <label className="label-text" htmlFor={`assertionConsumerLocation${fieldKey}`}>Location</label>
          <input type="text" id={`assertionConsumerLocation${fieldKey}`} className="text-field" value={assertionConsumersCopy[fieldKey].attributes.Location} onChange={(e) => {
            assertionConsumersCopy[fieldKey].attributes.Location = e.target.value
            setFieldValue("tags['md:AssertionConsumerService']", assertionConsumersCopy)
          }} />
        </div>
      </div>
    })}
    <button
      type="button"
      className="add-button"
      onClick={() => {
        const newKey = crypto.randomUUID()
        assertionConsumersCopy[newKey] = { attributes: { Binding: '', Location: '' }, name: "md:AssertionConsumerService" }
        setFieldValue('tags["md:AssertionConsumerService"]', assertionConsumersCopy)
      }}>Add Endpoint</button>
  </div>
}

const OrganizationInfoFields = (props: { values: FormShape, handleChange: HandleChangeType }) => {
  const { values, handleChange } = props
  return <div className="pl-8 flex flex-col gap-y-4">
    <div>
      <label htmlFor="organization.children[0].children" className="label-text">Name</label>
      <input type="text" className="text-field" value={values.tags["md:Organization"][0].children[0].children} name="tags['md:Organization'][0].children[0].children" id="organization.children[0].children" onChange={handleChange} />
    </div>
    <div>
      <label htmlFor="organization.children[1].children" className="label-text">Display Name</label>
      <input type="text" className="text-field" value={values.tags["md:Organization"][0].children[1].children} name="tags['md:Organization'][0].children[1].children" id="organization.children[1].children" onChange={handleChange} />
    </div>
    <div>
      <label htmlFor="organization.children[2].children" className="label-text">URL</label>
      <input type="text" className="text-field" value={values.tags["md:Organization"][0].children[2].children} name="tags['md:Organization'][0].children[2].children" id="organization.children[2].children" onChange={handleChange} />
    </div>
  </div>

}

const ContactInfoFields = (props: { values: FormShape, handleChange: HandleChangeType, setFieldValue: SetFieldValueType }) => {
  const { values, handleChange, setFieldValue } = props
  let contactCopy: ContactPerson = JSON.parse(JSON.stringify(props.values.tags["md:ContactPerson"]["0"]))
  return <div className="pl-8 flex flex-col gap-y-4">
    <div>
      <label className="label-text" htmlFor={"contactType"}>Contact Type</label>
      <select name={"contactType"}
        id={"contactType"}
        value={contactCopy.attributes.contactType}
        style={{ border: "1px solid #3a3a3a" }}
        onChange={(e) => {
          contactCopy.attributes.contactType = e.target.value
          setFieldValue("tags['md:ContactPerson'][0]", contactCopy)
        }}>
        <option value="" disabled hidden>Select Contact Type</option>
        {CONTACT_TYPES.map((type) => {
          return <option value={type} key={type}>{type}</option>
        })}
      </select>
    </div>
    <div>
      <label htmlFor="tags['md:ContactPerson'][0].children[0].children" className="label-text">Given Name</label>
      <input type="text" className="text-field" value={values.tags["md:ContactPerson"][0].children[0].children} name="tags['md:ContactPerson'][0].children[0].children" id="tags['md:ContactPerson'][0].children[0].children" onChange={handleChange} />
    </div>
    <div>
      <label htmlFor="tags['md:ContactPerson'][0].children[1].children" className="label-text">Surname</label>
      <input type="text" className="text-field" value={values.tags["md:ContactPerson"][0].children[1].children} name="tags['md:ContactPerson'][0].children[1].children" id="tags['md:ContactPerson'][1].children[1].children" onChange={handleChange} />
    </div>
    <div>
      <label htmlFor="tags['md:ContactPerson'][0].children[2].children" className="label-text">Email Address</label>
      <input type="text" className="text-field" value={values.tags["md:ContactPerson"][0].children[2].children} name="tags['md:ContactPerson'][0].children[2].children" id="tags['md:ContactPerson'][0].children[2].children" onChange={handleChange} />
    </div>
  </div>

}
export const FieldDropdown = (props: { Header: React.ReactElement, children?: React.ReactNode, closedAtStart?: boolean }) => {
  const [isFullDisplay, setIsFullDisplay] = useState(!Boolean(props.closedAtStart))
  const { Header } = props
  return <>
    <div className="flex items-center">
      <button className="border-none bg-transparent text-xl mr-4" type="button"
        onClick={() => {
          setIsFullDisplay((prevState) => {
            return !prevState
          })
        }}
      >{isFullDisplay ? <span>&#9660;</span> : <span>&#9654;</span>} </button>
      {Header}
    </div>
    {isFullDisplay ? props.children : null}
  </>
}

export const MetadataFormFields = (props: { values: FormShape, handleSubmit: (e?: FormEvent<HTMLFormElement> | undefined) => void, handleChange: HandleChangeType, setFieldValue: SetFieldValueType }) => {
  const { values, handleChange, setFieldValue } = props
  return <>
    <h2 className="text-5xl mb-8 text-center">Service Provider Metadata</h2>
    <form className="flex flex-col gap-y-8 pl-8">
      <div>
        <label htmlFor={"entityId"} className="w-72 label-text">Entity ID</label>
        <input type="text" className="text-field" name="attributes.entityId" id="entityId" value={values.attributes.entityId} onChange={handleChange} />
      </div>
      <FieldDropdown Header={<h3 className="text-4xl">Entity Attributes</h3>}>
        <ServiceFunctionalityAttributesFields values={values} setFieldValue={setFieldValue} />
      </FieldDropdown>
      <FieldDropdown Header={<h3 className="text-4xl">Organization Info</h3>}>
        <OrganizationInfoFields values={values} handleChange={handleChange} />
      </FieldDropdown>
      <FieldDropdown Header={<h3 className="text-4xl">Contact Person Info</h3>
      }>
        <ContactInfoFields values={values} handleChange={handleChange} setFieldValue={setFieldValue} />
      </FieldDropdown>
      <FieldDropdown closedAtStart Header={<h2 className="text-4xl">Advanced Info</h2>}>
        <div className="pl-12 flex flex-col gap-y-8">
          <FieldDropdown Header={<h3 className="text-4xl">Request Initiators</h3>}>
            <RequestInitiatorFields values={values} setFieldValue={setFieldValue} />
          </FieldDropdown>
          <FieldDropdown Header={<h3 className="text-4xl">Keys</h3>}>
            <KeyDescriptorsFields values={values} setFieldValue={setFieldValue} />
          </FieldDropdown>
          <FieldDropdown Header={<h3 className="text-4xl">Assertion Consumer Services</h3>}>
            <AssertionConsumerServicesFields values={values} setFieldValue={setFieldValue} />
          </FieldDropdown>
        </div>
      </FieldDropdown>
    </form>
  </>
}

const XmlTagDisplay = (props: { tag: XmlTag, children?: React.ReactNode }) => {
  let childDisplay: string | ReactNode[] | undefined | ReactNode
  const { name, attributes, children } = props.tag
  const keepSameLine = KEEP_SAME_LINE_TAGS.includes(name)
  if (typeof children === "object") {
    childDisplay = Object.keys(children).map((key) => {
      return <XmlTagDisplay tag={children[key]} key={key} />
    })
  } else if (typeof children === "string") {
    childDisplay = <p className={!keepSameLine ? "break-all" : ""}>{children}</p>
  }
  const isSelfClosing = typeof children !== "undefined"
  let attrsElems = attributes ? Object.keys(attributes).map((attrName) => {
    const newKey = crypto.randomUUID()
    if (attributes) {
      return <span key={newKey} style={{ color: "#1e95b9" }}> {attrName}=<span style={{ color: "#be7164" }}>{"\"" + attributes[attrName] + "\""}</span></span>
    }
  }) : null
  return <div className={"flex " + (!keepSameLine ? "flex-col" : "")}>
    <span style={{ color: "#3249c9" }}>{'<' + name}{attrsElems}{!isSelfClosing ? '/' : null}{'>'}</span>
    <div className={!keepSameLine ? "pl-12" : ''}>
      {childDisplay}
    </div>
    {isSelfClosing ?
      <span style={{ color: "#3249c9" }}>{'</' + name}{'>'}</span>
      :
      null
    }
  </div>

}

export const XmlDisplay = (props: { values: FormShape }) => {
  const entityDescriptorTag = createEntityDescTag(props.values)
  return <XmlTagDisplay tag={entityDescriptorTag} />
}
