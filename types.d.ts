


interface XmlTag {
    attributes: { [key: string]: string }
    name: string,
    children?: string | { [key: string]: XmlTag }
}

interface EntityAttributes extends XmlTag {
    children: { [key: string]: EntityAttribute }
}

interface TextTag extends XmlTag {
    children: string
}

interface EntityAttribute extends XmlTag {
    attributes: {
        Name: string,
        NameFormat: string
    }
    children: { [key: string]: TextTag }
}


interface UIInfo extends XmlTag {
    children: { [key: string]: UiInfoField }
}


interface AssertionConsumerService extends XmlTag {
    attributes: {
        Binding: string
        Location: string
        index?: string
    }
}


interface KeyDescriptor extends XmlTag {
    attributes: {
        use?: string
    }
    children: {
        0: KeyInfo,
        [key: string]: EncryptionMethod
    }
}

interface KeyInfo extends XmlTag {
    children: {
        [key: string]: TextTag | X509Data
    }
}

interface EncryptionMethod extends XmlTag {
    attributes: {
        Algorithm: string
    }
}

interface X509Data extends XmlTag {
    children: {
        [key: string]: TextTag
    }
}

interface OrgInfoShape extends XmlTag {
    children: {
        [key: string]: TextTag
    }
}

interface ContactPerson extends XmlTag {
    attributes:{
        contactType:string
    }
    children:{
        [key: string]: TextTag
    }
}
interface RequestInitiator extends XmlTag {
    attributes:{
        Binding: "urn:oasis:names:tc:SAML:profiles:SSO:request-init",
        Location: string
    }
}
interface FormShape {
    tags: {
        "mdattr:EntityAttributes": { "0": EntityAttributes }
        "md:KeyDescriptor": { [key: string]: KeyDescriptor }
        "md:AssertionConsumerService": { [key: string]: AssertionConsumerService }
        "md:Organization": { "0": OrgInfoShape }
        "md:ContactPerson": {
            "0": ContactPerson
        }
        "init:RequestInitiator": {
            [key: string]: RequestInitiator
        }
    }
    attributes: {
        entityId: string
    }
}

type HandleChangeType = {
    (e: ChangeEvent<any>): void;
    <T = string | ChangeEvent<any>>(field: T): T extends ChangeEvent<any> ? void : (e: string | ChangeEvent<any>) => void;
}

type SetFieldValueType = (field: string, value: any, shouldValidate?: boolean | undefined) => Promise<void | FormikErrors<FormShape>>
