This project is to provide a SP registry app.

## Setup Instructions.

1. Install Node.js (https://nodejs.org/en) and Git (https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
2. Open up your terminal, and navigate in the terminal to the folder where you want the code for this repository to be stored. Then run the command `git clone https://gitlab.com/utsso/sp-registry.git` in the terminal and input your credentials for GitLab to download the code onto your computer. This should create a folder called `sp-registry` in the folder you navgiated to in the terminal.
3. In the terminal, navigate into the `sp-registry` folder you just created. Then run `npm install` in the terinal.
4. Run `npm run dev` in the terminal to start the development server, and you should be able to view the app and try it out by going to `localhost:3000` in a web browser of your choice.
5. To stop the app, close the terminal. To run the app after the initial setup, you just need to navigate to the `sp-registry` folder in the terminal, run `npm run dev` in the terminal and go to `localhost:3000`.